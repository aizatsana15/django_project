
__all__ = [
    'BlogConfig'
]

from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'blog'
