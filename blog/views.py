from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.views.decorators.cache import cache_page
import logging

from .models import Post

logger = logging.getLogger('blog')


def home(request):
    """
    View displays last posts on home page paginated by 5
    """
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'blog/home.html', context)


class PostListView(ListView):
    """
    View displays lists posts 5 per page
    """
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 5


class UserPostListView(ListView):
    """
    View displays lists posts specific user paginated by 5
    """
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')


@method_decorator(cache_page(24*60*60), name='dispatch')
class PostDetailView(DetailView):
    """
    View displays posts detail
    """
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        logger.info(f"{self.request.user} successfuly created post")
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """
    View displays updating form for existing posts
    """
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        logger.info(f"Post was updated by {self.request.user}")
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    """
    View displays the delete form for post current user
    """
    model = Post
    success_url = '/'

    def test_func(self):
        """
        The method checks if the current user is the author of the post
        """
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


def about(request):
    """
    View displays info about current blog
    """
    return render(request, 'blog/about.html', {'title': 'About'})
