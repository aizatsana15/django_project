from django.test import TestCase
from model_mommy import mommy

from blog.models import Post


class PostModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # author = mommy.make('User')
        # Post.objects.create(title='TestPost', content='Today foggy wheather', author_id=author.id)
        mommy.make('Post')

    def test_title_max_length(self):
        post = Post.objects.last()
        max_length = post._meta.get_field('title').max_length
        self.assertEquals(max_length, 100)

    def test_get_absolute_url(self):
        post = Post.objects.last()
        self.assertEquals(post.get_absolute_url(), f'/blog/post/{post.id}/')
