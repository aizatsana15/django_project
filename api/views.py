from rest_framework import viewsets, permissions

from .serializers import PostSerializers
from blog.models import Post


class PostSerializersView(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializers
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
