from django.urls import path, include
from rest_framework import routers

from .views import PostSerializersView

router = routers.DefaultRouter()
router.register('posts', PostSerializersView)

urlpatterns = [
    path('', include(router.urls))
]
