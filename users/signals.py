from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver

import logging

from .models import Profile
logger = logging.getLogger('blog')


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        logger.info(f'Prepare to create user profile with profile_id = {instance.pk}')
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    logger.info(f'Save user profile with profile_id = {instance.pk}')
    instance.profile.save()
