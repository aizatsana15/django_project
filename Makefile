runserver:
	@$ python3 manage.py runserver --settings=django_project.settings

migrations:
	@$ python3 manage.py makemigrations --settings=django_project.settings

migrate:
	@$ python3 manage.py migrate --settings=django_project.settings