## Сontents
* [Name](#name)
* [About](#about)
* [Installation](#Installation)

## Name
Django Blog
## About

Django Blog is a simple blog where you can create your own posts about what you want.

You can find life site [here](https://mynewestdjangoblog.herokuapp.com/).

## Installation  
To run this project:
```
$ git clone https://gitlab.com/aizatsana15/django_project.git
$ pip3 install -r requirements.txt
$ python3 manage.py migrate
$ python3 manage.py loaddata fixtures.json  

```

